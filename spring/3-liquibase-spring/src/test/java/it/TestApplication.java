package it;

import com.example.DemoApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.Base64Utils;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = DemoApplication.class)
@DirtiesContext
@AutoConfigureMockMvc
public class TestApplication {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void contextLoads() throws Exception {
        this.mockMvc.perform(get("/")).andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(containsString("Hello World")));
    }

    @Test
    public void securePageReturns401() throws Exception {
        this.mockMvc.perform(get("/secure/")).andDo(print()).andExpect(status().is(HttpStatus.UNAUTHORIZED.value()));
    }

    @Test
    public void secureReturnsPrincipalName() throws Exception {
        this.mockMvc.perform(get("/secure/").header(HttpHeaders.AUTHORIZATION,
                "Basic " + Base64Utils.encodeToString("test:testtest".getBytes())))
                .andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(containsString("Your profile: <span>test</span>")));
    }

}