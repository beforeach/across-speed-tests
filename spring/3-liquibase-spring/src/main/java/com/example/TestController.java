package com.example;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import javax.sql.DataSource;
import java.util.List;
import java.util.Map;

//import org.springframework.jdbc.core.JdbcTemplate;

@Controller
public class TestController {

    @Autowired
    private DataSource dataSource;

    @GetMapping("/")
    public String hello() {
        List<Map<String, Object>> list = new JdbcTemplate(dataSource).queryForList("select * from company");
        // Make sure the liquibase executed
        assert list.size() == 0;
        return "hello";
    }
}
