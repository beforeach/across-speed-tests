package com.example.company;

import org.springframework.data.jpa.domain.AbstractPersistable;

import javax.persistence.Entity;

@Entity
public class Company extends AbstractPersistable<Long> {

    private String name;


    public Company() {
        this(null);
    }

    /**
     * Creates a new employee instance.
     */
    public Company(Long id) {
        this.setId(id);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
