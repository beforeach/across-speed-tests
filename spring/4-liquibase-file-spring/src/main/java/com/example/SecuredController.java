package com.example;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.security.Principal;

@Controller
public class SecuredController {
    @GetMapping("/secure")
    public String profile(Model model, Principal principal) {
        model.addAttribute("principal", principal);
        return "profile";
    }
}
