package com.example;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import javax.sql.DataSource;

//import org.springframework.jdbc.core.JdbcTemplate;

@Controller
public class TestController {

    @Autowired
    private DataSource dataSource;

    @GetMapping("/")
    public String hello() {
        assert new JdbcTemplate(dataSource).queryForList("select * from company").size() == 0;
        for (int i = 1; i <= 5; i++) {
            assert new JdbcTemplate(dataSource).queryForList("select * from table" + i).size() == 0;
        }
        return "hello";
    }
}
