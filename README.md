## Across Speed Tests

Inspired by the Spring Boot Startup benchmarks at: https://github.com/dsyer/spring-boot-startup-bench

### Usage

Build the jars and execute their unit tests: *mvn clean package* 

```
$ mvn clean package
```

Execute the benchmarks

```
$ cd benchmarks
$ java -jar target/benchmarks.jar
```

### Testing other versions

The project contains 2 profiles:

* across2 (benchmarks run Across Platform 2.1.2.RELEASE and 1.5.17.RELEASE for Spring)
* across3 (benchmarks run Across Platform 3.0.0-SNAPSHOT and 2.0.6.RELEASE for Spring)

By default, the *across3* profile will be activated. To test other version just use:

```
$ mvn -Pacross2 clean package && cd benchmarks && java -jar target/benchmarks.jar
```

Note that you must try and keep binary and configuration compatibility, also for major releases.

For this release, you may put utility code in the *utilities* module.
