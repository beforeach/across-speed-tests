package com.example;

import org.openjdk.jmh.annotations.*;

@Measurement(iterations = 5)
@Warmup(iterations = 1)
@Fork(value = 2, warmups = 0)
@BenchmarkMode(Mode.AverageTime)
public class Test_03 {

    @Benchmark
    public void Liquibase_Spring_and_Jdbc(LiquibaseSpring state) throws Exception {
        state.run();
    }

    @Benchmark
    public void Liquibase_Across(LiquibaseAcross state) throws Exception {
        state.run();
    }

    @State(Scope.Benchmark)
    public static class LiquibaseSpring extends ProcessLauncherState {
        public LiquibaseSpring() {
            super("../spring/3-liquibase-spring/target", "-jar",
                    jarFile("com.example:3-liquibase-spring:jar:spring:0.0.1-SNAPSHOT"),
                    "--server.port=0");
        }

        @TearDown(Level.Iteration)
        public void stop() throws Exception {
            super.after();
        }
    }

    @State(Scope.Benchmark)
    public static class LiquibaseAcross extends ProcessLauncherState {
        public LiquibaseAcross() {
            super("../across/3-liquibase-across/target", "-jar",
                    jarFile("com.example:3-liquibase-across:jar:across:0.0.1-SNAPSHOT"),
                    "--server.port=0");
        }

        @TearDown(Level.Iteration)
        public void stop() throws Exception {
            super.after();
        }
    }
}
