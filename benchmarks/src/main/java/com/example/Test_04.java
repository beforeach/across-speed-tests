package com.example;

import org.openjdk.jmh.annotations.*;

@Measurement(iterations = 5)
@Warmup(iterations = 1)
@Fork(value = 2, warmups = 0)
@BenchmarkMode(Mode.AverageTime)
public class Test_04 {

    @Benchmark
    public void Liquibase_FileDb_Spring_and_Jdbc(LiquibaseFileDbSpring state) throws Exception {
        state.run();
    }

    @Benchmark
    public void Liquibase_FileDb_Across(LiquibaseFileDbAcross state) throws Exception {
        state.run();
    }

    @State(Scope.Benchmark)
    public static class LiquibaseFileDbSpring extends ProcessLauncherState {
        public LiquibaseFileDbSpring() {
            super("../spring/4-liquibase-file-spring/target", "-jar",
                    jarFile("com.example:4-liquibase-file-spring:jar:spring:0.0.1-SNAPSHOT"),
                    "--server.port=0");
        }

        @TearDown(Level.Iteration)
        public void stop() throws Exception {
            super.after();
        }
    }

    @State(Scope.Benchmark)
    public static class LiquibaseFileDbAcross extends ProcessLauncherState {
        public LiquibaseFileDbAcross() {
            super("../across/4-liquibase-file-across/target", "-jar",
                    jarFile("com.example:4-liquibase-file-across:jar:across:0.0.1-SNAPSHOT"),
                    "--server.port=0");
        }

        @TearDown(Level.Iteration)
        public void stop() throws Exception {
            super.after();
        }
    }
}
