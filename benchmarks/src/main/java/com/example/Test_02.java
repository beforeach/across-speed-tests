package com.example;

import org.openjdk.jmh.annotations.*;

@Measurement(iterations = 5)
@Warmup(iterations = 1)
@Fork(value = 2, warmups = 0)
@BenchmarkMode(Mode.AverageTime)
public class Test_02 {

    @Benchmark
    public void Security_Spring(SecuritySpring state) throws Exception {
        state.run();
    }

    @Benchmark
    public void Security_Across(SecurityAcross state) throws Exception {
        state.run();
    }

    @State(Scope.Benchmark)
    public static class SecuritySpring extends ProcessLauncherState {
        public SecuritySpring() {
            super("../spring/2-security-spring/target", "-jar",
                    jarFile("com.example:2-security-spring:jar:spring:0.0.1-SNAPSHOT"),
                    "--server.port=0");
        }

        @TearDown(Level.Iteration)
        public void stop() throws Exception {
            super.after();
        }
    }

    @State(Scope.Benchmark)
    public static class SecurityAcross extends ProcessLauncherState {
        public SecurityAcross() {
            super("../across/2-security-across/target", "-jar",
                    jarFile("com.example:2-security-across:jar:across:0.0.1-SNAPSHOT"),
                    "--server.port=0");
        }

        @TearDown(Level.Iteration)
        public void stop() throws Exception {
            super.after();
        }
    }
}
