package com.example;

import org.openjdk.jmh.annotations.*;

@Measurement(iterations = 5)
@Warmup(iterations = 1)
@Fork(value = 2, warmups = 0)
@BenchmarkMode(Mode.AverageTime)
public class Test_01 {

    @Benchmark
    public void Web_Spring(Test1WebSpring state) throws Exception {
        state.run();
    }

    @Benchmark
    public void Web_Across(Test1WebAcross state) throws Exception {
        state.run();
    }

    @State(Scope.Benchmark)
    public static class Test1WebSpring extends ProcessLauncherState {
        public Test1WebSpring() {
            super("../spring/1-web-spring/target", "-jar",
                    jarFile("com.example:1-web-spring:jar:spring:0.0.1-SNAPSHOT"),
                    "--server.port=0");
        }

        @TearDown(Level.Iteration)
        public void stop() throws Exception {
            super.after();
        }
    }

    @State(Scope.Benchmark)
    public static class Test1WebAcross extends ProcessLauncherState {
        public Test1WebAcross() {
            super("../across/1-web-across/target", "-jar",
                    jarFile("com.example:1-web-across:jar:across:0.0.1-SNAPSHOT"),
                    "--server.port=0");
        }

        @TearDown(Level.Iteration)
        public void stop() throws Exception {
            super.after();
        }
    }
}
