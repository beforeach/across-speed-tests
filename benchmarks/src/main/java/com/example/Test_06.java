package com.example;

import org.openjdk.jmh.annotations.*;

@Measurement(iterations = 5)
@Warmup(iterations = 1)
@Fork(value = 2, warmups = 0)
@BenchmarkMode(Mode.AverageTime)
public class Test_06 {

    @Benchmark
    public void Starter_Jpa_Spring(StarterJpaSpring state) throws Exception {
        state.run();
    }

    @Benchmark
    public void HibernateJpaModuleAcross(HibernateJpaModuleAcross state) throws Exception {
        state.run();
    }

    @State(Scope.Benchmark)
    public static class StarterJpaSpring extends ProcessLauncherState {
        public StarterJpaSpring() {
            super("../spring/5-starter-jpa-spring/target", "-jar",
                    jarFile("com.example:5-starter-jpa-spring:jar:spring:0.0.1-SNAPSHOT"),
                    "--server.port=0");
        }

        @TearDown(Level.Iteration)
        public void stop() throws Exception {
            super.after();
        }
    }

    @State(Scope.Benchmark)
    public static class HibernateJpaModuleAcross extends ProcessLauncherState {
        public HibernateJpaModuleAcross() {
            super("../across/6-hibernate-jpa-module-across/target", "-jar",
                    jarFile("com.example:6-hibernate-jpa-module-across:jar:across:0.0.1-SNAPSHOT"),
                    "--server.port=0");
        }

        @TearDown(Level.Iteration)
        public void stop() throws Exception {
            super.after();
        }
    }
}
