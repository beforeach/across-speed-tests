package com.example;

import org.springframework.boot.SpringBootVersion;
import org.springframework.util.StringUtils;

public class Utils {
    public static String backwardsCompatibleSpringSecurityPassword() {
        return StringUtils.startsWithIgnoreCase(SpringBootVersion.getVersion(), "1.") ? "testtest" : "{noop}testtest";
    }
}