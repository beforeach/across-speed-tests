package com.example.application.config;

import com.example.application.domain.DomainMarker;
import com.foreach.across.modules.hibernate.jpa.repositories.config.EnableAcrossJpaRepositories;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableAcrossJpaRepositories(basePackageClasses = DomainMarker.class)
public class DomainConfiguration {
}