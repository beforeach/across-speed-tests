package com.example.application.domain.company;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CompanyRepository extends CrudRepository<Company, Long> {

    List<Company> findByName(String name);

    @Query("select c from Company c where c.name = :name")
    List<Company> findByNameQuery(@Param("name") String name);
}