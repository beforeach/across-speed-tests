package com.example.application.domain.employee;

import org.springframework.data.jpa.domain.AbstractPersistable;

import javax.persistence.Entity;

@Entity
public class Employee extends AbstractPersistable<Long> {

    private String firstname;
    private String lastname;

    public Employee() {
        this(null);
    }

    /**
     * Creates a new employee instance.
     */
    public Employee(Long id) {
        this.setId(id);
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }
}
