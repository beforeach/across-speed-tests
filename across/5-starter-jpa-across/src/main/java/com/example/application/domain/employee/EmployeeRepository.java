package com.example.application.domain.employee;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EmployeeRepository extends CrudRepository<Employee, Long> {

    List<Employee> findByLastname(String lastname);

    @Query("select u from Employee u where u.firstname = :firstname")
    List<Employee> findByFirstname(@Param("firstname") String firstname);

    @Query("select u from Employee u where u.firstname = :name or u.lastname = :name")
    List<Employee> findByFirstnameOrLastname(String name);
}