package com.example;

import com.foreach.across.config.AcrossApplication;
import com.foreach.across.modules.spring.security.SpringSecurityModule;
import com.foreach.across.modules.web.AcrossWebModule;
import org.springframework.boot.SpringApplication;

@AcrossApplication(modules = {AcrossWebModule.NAME, SpringSecurityModule.NAME})
public class DemoApplication {
    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }
}
