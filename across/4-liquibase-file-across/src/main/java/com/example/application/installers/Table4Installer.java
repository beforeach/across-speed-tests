package com.example.application.installers;

import com.foreach.across.core.annotations.Installer;
import com.foreach.across.core.installers.AcrossLiquibaseInstaller;
import com.foreach.across.core.installers.InstallerRunCondition;

@Installer(description = "Installs a table schema", runCondition = InstallerRunCondition.VersionDifferent, version = 1)
public class Table4Installer extends AcrossLiquibaseInstaller {
}
