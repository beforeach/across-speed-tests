package com.example.application.config;

import com.example.Utils;
import com.foreach.across.modules.spring.security.configuration.SpringSecurityWebConfigurerAdapter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;

@Configuration
@ConditionalOnClass(SpringSecurityWebConfigurerAdapter.class)
public class SecurityConfiguration extends SpringSecurityWebConfigurerAdapter {

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.antMatcher("/secure/**").authorizeRequests().anyRequest().hasAnyRole("ADMIN", "USER").
                and().httpBasic().and().formLogin().disable();
    }

    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication().withUser("test").password(Utils.backwardsCompatibleSpringSecurityPassword()).roles("ADMIN");
    }
}