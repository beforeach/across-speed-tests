package it;

import com.example.DemoApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.Base64Utils;

import java.util.Objects;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {DemoApplication.class}, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@DirtiesContext
public class TestApplication {
    @Value("${local.server.port}")
    private int port;
    private TestRestTemplate restTemplate = new TestRestTemplate();

    @Test
    public void contextLoads() {
        String output = restTemplate.getForObject("http://localhost:" + port + "/",
                String.class);
        assertTrue(output.contains("Hello World"));
    }

    @Test
    public void securePageReturns401() {
        ResponseEntity<String> output = restTemplate.getForEntity("http://localhost:" + port + "/secure",
                String.class);
        assertEquals(401, output.getStatusCodeValue());
    }

    @Test
    public void secureReturnsPrincipalName() {
        HttpHeaders headers = new HttpHeaders();
        headers.set(HttpHeaders.AUTHORIZATION, "Basic " + Base64Utils.encodeToString("test:testtest".getBytes()));
        HttpEntity entity = new HttpEntity(headers);
        ResponseEntity<String> response = restTemplate.exchange(
                "http://localhost:" + port + "/secure", HttpMethod.GET, entity, String.class);
        assertTrue(Objects.requireNonNull(response.getBody()).contains("Your profile: <span>test</span>"));
    }

}